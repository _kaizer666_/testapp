<?php
	namespace App\Interfaces;
	
	interface SourceInterface {
		/**
		 * @param string $inn
		 * @return array|bool
		 */
		public function getOrgByInn(string $inn);
		
		/**
		 * @param array|bool $response
		 * @param string $source
		 * @return boolean
		 */
		public function incrementStat($response, $source) : bool;
	}
